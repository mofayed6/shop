<?php
namespace App\Service;
use App\Repository\ProductRepository;

class Slugger
{
    private $productRepository;
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function slugify($product): string
    {
        $name = $product->getName();
        $slug = str_replace(' ', '-', $name);
        return $slug;
    }
}