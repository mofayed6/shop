<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Mapping\ClassMetadata;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @ORM\Table(
 *     name="products",
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(name="uniq_slug_softdelete", columns={"slug", "deleted_at"})
 *     }
 * )
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string")
     */
    private $category;

    /**
     * @ORM\Column(type="integer")
     */
    private $stock;

    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    private $price;

    /**
     * @ORM\Column(type="decimal", scale=3)
     */
    private $weight;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Image", mappedBy="product", cascade={"persist"})
     */
    private $images;
    private $quantity;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateCreated;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;
    /**
     * @ORM\Column(type="string", length=191)
     */
    private $slug;


    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->dateCreated = new \DateTime();
        $this->deletedAt = date_create('0000-00-00 00:00:00');
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('name', new Assert\Type('string'));
        $metadata->addPropertyConstraint('name', new Assert\NotNull());

        $metadata->addPropertyConstraint('description', new Assert\Type('string'));
        $metadata->addPropertyConstraint('description', new Assert\NotNull());

        $metadata->addPropertyConstraint('category', new Assert\Type('string'));
        $metadata->addPropertyConstraint('category', new Assert\NotNull());

        $metadata->addPropertyConstraint('stock', new Assert\Type('int'));
        $metadata->addPropertyConstraint('stock', new Assert\NotNull());

        $metadata->addPropertyConstraint('price', new Assert\NotNull());

        $metadata->addPropertyConstraint('weight', new Assert\NotNull());

        $metadata->addPropertyConstraint('images', new Assert\Count([
            'min' => 1,
            'max' => 3,
            'minMessage' => 'Chaque produit doit avoir au moins une image',
            'maxMessage' => 'Un produit ne peut pas avoir plus de trois images'
        ]));
        $metadata->addPropertyConstraint('images', new Assert\Valid());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }


    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @return mixed
     */
    public function getStock(): ?int
    {
        return $this->stock;
    }

    /**
     * @return mixed
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getWeight(): ?float
    {
        return $this->weight;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }


    /**
     * @return mixed
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }
    /**
     * @param int $quantity
     * @return Product
     */
    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category): void
    {
        $this->category = $category;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @param mixed $images
     */
    public function setImages($images): void
    {
        $this->images = $images;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @param mixed $stock
     */
    public function setStock($stock): void
    {
        $this->stock = $stock;
    }


    /**
     * @param string $slug
     * @return Product
     */
    public function setSlug(string $slug): self
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @param mixed $weight
     */
    public function setWeight($weight): void
    {
        $this->weight = $weight;
    }


    /**
     * @return mixed
     */
    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }


    /**
     * @param \DateTimeInterface|null $deletedAt
     * @return Product
     */
    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateCreated(): ?\DateTimeInterface
    {
        return $this->dateCreated;
    }

    /**
     * @param \DateTimeInterface $dateCreated
     * @return Product
     */
    public function setDateCreated(\DateTimeInterface $dateCreated): self
    {
        $this->dateCreated = $dateCreated;
        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }
    public function setActive(bool $active): self
    {
        $this->active = $active;
        return $this;
    }


    public function addImage(Image $image)
    {
        $image->setProduct($this);

        $this->images->add($image);
    }
    public function removeImage(Image $image)
    {
        $this->images->remove($image);
    }


    public function calcTotalPrice(): float
    {
        return $this->quantity * $this->price;
    }

    public function hasStock(): bool
    {
        return $this->stock > 0;
    }

}
